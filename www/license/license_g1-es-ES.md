Licencia Ğ1 - v0.2.8
===================

:fecha: 2017-04-04 12:59
:modificación: 2018-02-24 09:30

**Licencia de la moneda y compromiso de responsabilidad.**

Cualquier certificación de un nuevo miembro de Ğ1 debe ir primero acompañada por la transmisión de esta licencia de moneda Ğ1 la cual el certificador debe garantizar que ha sido estudiada, entendida y aceptada por la persona que será certificada.

Cualquier evento de encuentro relacionado con Ğ1 debe ir acompañado de la transmisión de esta licencia, que puede leerse en voz alta y transmitirse por cualquier medio.

Red de confianza Ğ1 (RdC Ğ1)
------------------------------

** Aviso de advertencia :** Certificar no solo es asegurarte de que has conocido a la persona, es asegurar a la comunidad Ğ1 que conoces a la persona lo suficientemente bien y que sabes cómo contactarla fácilmente, y ser capaz de identificar una cuenta duplicada realizada por una persona que has certificado, u otros tipos de problemas (desaparición ...), haciendo verificaciones cruzadas que revelan el problema en caso de que lo haya.

**Consejos muy recomendados**

Bien conocer a una persona significa que puedes comunicarte con ella por varios medios distintos (físicos, electrónicos, otros ...) pero también que conoces a varias personas que la conocen bien y, por lo tanto, pueden comunicarse con ella igualmente. Especialmente si no conoces bien ninguno de sus otros certificadores, es una clara indicación de que no conoces bien a la persona; una certificación de este tipo provoca una alerta hacia toda la comunidad Ğ1. En caso de conocimiento insuficiente, es importante NO certificar.

Nunca certifiques solo, sino que acompañado por al menos otro miembro de la RdC Ğ1 para evitar cualquier mal manejo. En caso de error, advierte a otros miembros de la RdC Ğ1 inmediatamente.

Antes de cualquier certificación, asegúrate de verificar si su cuenta (tanto si se esté validando como ya sea miembro) ha recibido ya una o más certificaciones. Si es necesario, solicite información para ponerte en contacto con estos otros certificadores para verificar juntos que conoces bien la persona en cuestión por la creación de la nueva cuenta, así como la clave pública correspondiente.

Comprueba que la persona a punta de ser certificada domina el manejo su cuenta: una buena manera de comprobar esto es transferir unos Ğ1 a la cuenta de destino, y luego pedir la vuelta a tu propia cuenta. Esto asegura el control eficaz, por el futuro certificado, de su clave privada.

Compruebe que tus contactos hayan estudiado y comprendido a fondo la licencia Ğ1 actualizada.

Si te das cuenta de que un certificador real o potencial de la cuenta en cuestión no conoce a la persona interesada, avise inmediatamente a los expertos del tema dentro de tus contactos de la RdC Ğ1, para que la RdC Ğ1 verifique el procedimiento de validación.

Cuando eres miembro de la RdC Ğ1 y estás a punto de certificar una nueva cuenta:


** Estás seguro: **

1 °) de conocer lo suficiente (no solo "de visu") la persona que declara administrar esta clave pública (nueva cuenta). Vea los consejos altamente recomendados más arriba para asegurarte de que conoces "bien".

2) Haber comprobado personalmente con ella que esta es la clave pública que está a punto de certificar (consulte las sugerencias anteriores).

3°) Haber verificado con la persona interesada que ha generado su documento de revocación de cuenta Duniter que le permitirá, si es necesario, desactivar su estado de miembro (caso de robo de cuenta, un cambio de identificación, una cuenta creada incorrectamente, etc.).

4a °) De haberte encontrado con la persona físicamente para asegurarte de que la conoces bien y quién es ella que administra esta clave pública.

4b °) O verificar de forma remota el enlace entre la clave pública y la persona, contactando a esta persona por diferentes medios de comunicación, como correo en papel + red social + foro + correo electronico + video conferencia + teléfono (reconocer la voz). Porque si se puede piratear una cuenta de correo electrónico o una cuenta de foro, es mucho más difícil imaginar piratear cuatro medios de comunicación separados, e imitar la apariencia (video) y la voz de la persona además.

El 4a °) sin embargo, es preferible a 4b °), mientras que los puntos 1 °) 2 °) y 3 °) son previamente indispensables.

**Reglas abreviadas de la RdC :**

Cada miembro tiene un stock de 100 posibles certificaciones, que solo puede emitir a razón de 1 certificación cada 5 días.

Válido por 2 meses, se adopta definitivamente una certificación para un nuevo miembro siesta persona tiene al menos otras 4 certificaciones al cabo de estos 2 meses, de lo contrario, el proceso de inscripción deberá reiniciarse.

Para convertirse en un nuevo miembro de la RdC Ğ1 es necesario obtener 5 certificaciones y y estar a una distancia <= 5 pasos del 80% de los referentes de la RdC.

Un miembro de la RdC Ğ1 es un miembro referente cuando ha recibido y emitido al menos Y [N] certificaciones donde N es el número de miembros RdC e Y [N] = techo N ^ (1/5). Ejemplos:

* Para 1024 < N ≤ 3125 tenemos Y[N] = 5
* Para 7776 < N ≤ 16807 tenemos Y[N] = 7
* para 59049 < N ≤ 100 000 tenemos Y[N] = 10

Una vez que el nuevo miembro participa en  la RdC Ğ1, sus certificaciones seguirán siendo válidas durante 2 años.

Para seguir siendo miembro, debe renovar su acuerdo regularmente con su clave privada (cada 12 meses) y asegurarse de que siempre tenga al menos 5 certificaciones válidas más de allá de los 2 años.

Moneda Ğ1
----------

Ğ1 se genera a través de un Dividendo Universal (DU) para cada ser humano en el Anillo de Confianza Ğ1, que tiene la siguiente forma:

* 1 DU por persona y por día.

**Codigo de la moneda Ğ1**

La cantidad en Ğ1 del DU es idéntica cada día hasta el próximo equinoccio en el que se re-evaluará el DU según la fórmula (con 1 día = 86,400 segundos): :

* DU día (próximo equinoccio) = DU día(equinoccio) + c² (M/N)(equinoccio) / (182,625 días)

Y como parámetros:

* c = 4,88% / equinoccio
* DU(0) = 10,00 Ğ1

Y como variables :

* *M* la oferta de dinero o masa monetaria total en el equinoccio
* *N* el número de miembros al día del equinoccio

Software Ğ1 y licencia Ğ1
--------------------------

El software Ğ1 que permite a los usuarios administrar su uso de Ğ1 debe transmitir esta licencia con el software y todos los parámetros técnicos de la moneda Ğ1 y de la RdC Ğ1 que se ingresan en el bloque 0 de Ğ1. El software que no cumple con estas obligaciones de la licencia no es compatible con Ğ1.

Para obtener más informaciones en los detalles técnicos, es posible consultar directamente el código de Duniter, que es un software libre así como los datos de la blockchain Ğ1, recuperándolos a través de una instancia de Duniter Ğ1 (o nodo).

Más información en el sitio web del equipo de Duniter https://www.duniter.org